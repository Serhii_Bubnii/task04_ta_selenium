package com.epam.bubnii.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverManager {

    private static Logger LOGGER = LogManager.getLogger(WebDriverManager.class);
    private static final String WEB_DRIVER_PATH = "src/main/resources/chromedriver.exe";
    private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";
    private static WebDriver webDriver;

    static {
        System.setProperty(WEB_DRIVER_NAME, WEB_DRIVER_PATH);
    }

    private WebDriverManager(){
    }

    public static WebDriver getWebDriver(){
        if (webDriver == null){
            initWebDriver();
        }
        LOGGER.info("Get web driver");
        return webDriver;
    }

    private static WebDriver initWebDriver(){
        LOGGER.info("Web driver initialize");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return webDriver;
    }
}
