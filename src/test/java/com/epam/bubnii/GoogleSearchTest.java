package com.epam.bubnii;

import com.epam.bubnii.utils.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.util.List;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class GoogleSearchTest {

    private static final Logger LOGGER = LogManager.getLogger(GoogleSearchTest.class);
    private static final String HOME_PAGE_GOOGLE = "https://www.google.com/";
    private static WebDriver webDriver;

    @BeforeMethod
    public void homePageLoad(){
        LOGGER.info("Load home page by url" + HOME_PAGE_GOOGLE);
        webDriver = WebDriverManager.getWebDriver();
        webDriver.get(HOME_PAGE_GOOGLE);
    }

    @Test
    public void testGoogleSearch(){
        WebElement searchInput = webDriver.findElement(By.name("q"));
        searchInput.sendKeys("Apple");
        searchInput.submit();

        webDriver.findElement(By.cssSelector(".q.qs[href*='&tbm=isch']")).click();

        List<WebElement> resultImages = webDriver.findElements(By.xpath("//*[@id='search']//*[@data-row]//img"));
        assertFalse(resultImages.isEmpty(), "Result images list is empty");
        assertTrue(resultImages.stream().anyMatch(WebElement::isDisplayed),
                "No visible images on page");
    }

    @AfterMethod
    public void destroyWebDriver(){
        LOGGER.info("Destroy Chrome driver");
        webDriver.quit();
    }
}
